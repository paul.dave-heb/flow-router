//
//  CouponFlow.swift
//  FlowRouters
//
//  Created by Paul,Dave on 11/1/23.
//

import SwiftUI

struct CouponFlow: View {
    let couponService: CouponService
    let initialCoupon: Coupon

    @Environment(\.router) var router

    var body: some View {
        PublisherView(publisher: couponService.couponPublisher(for: initialCoupon.id), initialValue: initialCoupon) { coupon in
            CouponView(coupon: coupon, popHandler: { router.popToRoot() }, dismissHandler: { router.dismissAll() })
        }.onAppear {
            print("Coupon Flow: \(router.description)")
        }
    }
}

struct CouponView: View {
    let coupon: Coupon

    var popHandler: () -> Void
    var dismissHandler: () -> Void

    var body: some View {
        ZStack {
            Color.yellow
            VStack {
                Text("Coupon \(coupon.id)")
                Button(action: { popHandler() }, label: {
                    Text("Pop to root")
                })
                Button(action: { dismissHandler() }, label: {
                    Text("Dismiss")
                })
            }
        }
    }
}
