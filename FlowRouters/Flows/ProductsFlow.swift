//
//  ProductsFlow.swift
//  FlowViews
//
//  Created by Paul,Dave on 10/28/23.
//

import SwiftUI

// Flows do not contain NavigationStack, but they do handle navigation
struct ProductsFlow: View {
    private enum Path: Hashable {
        case detail(Product)
        case couponFlow(Coupon)
    }

    struct Services {
        let cartService: CartService
        let couponService: CouponService
        let productService: ProductService
    }

    @Environment(\.router) var router

    let services: Services
    var product: Product?
    let couponTapHandler: (Coupon) -> Void // The ProductsFlow doesn't handle coupons

    var body: some View {

        PublisherView(publisher: services.productService.productListPublisher().optional) {
            ProductListView(products: $0) { product in
                router.append(Path.detail(product))
            }
        }

        // detail view
        .navigationDestination(for: Path.self) { path in
            switch path {
            case .detail(let product):
                PublisherView(publisher: services.productService.productPublisher(for: product.id), initialValue: product) {
                    ProductDetailView(product: $0, couponTapHandler: couponTapHandler)
                }
            case .couponFlow(let coupon): 
                CouponFlow(couponService: services.couponService, initialCoupon: coupon).routerSegment()
            }

        }
        .onAppear {
            print(router.description)
        }
    }
}

// Content View - no services, simple value params
struct ProductListView: View {
    let products: [Product]?
    let tapHandler: (Product) -> Void

    var body: some View {
        Group {
            if let products {
                List(products) { product in
                    Button(action: {
                        tapHandler(product)
                    }, label: {
                        Text("Product \(product.id)")
                    })
                }
            } else {
                Text("loading")
            }
        }.navigationTitle("Products")
    }
}

// Content View - no services, simple value params
struct ProductDetailView: View {
    let product: Product
    let couponTapHandler: (Coupon) -> Void

    var body: some View {
        ZStack {
            Color.green
            VStack {
                Text("Product \(product.id)")
                Button("This is a coupon", action: {
                    couponTapHandler(Coupon(id: 72))
                })
            }

        }
        .navigationTitle("Product \(product.id)")
    }
}
