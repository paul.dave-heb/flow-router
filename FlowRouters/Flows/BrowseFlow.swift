//
//  BrowseFlow.swift
//  FlowRouters
//
//  Created by Paul,Dave on 11/1/23.
//

import SwiftUI

let cartService = CartService()
let productService = ProductService()
let couponService = CouponService()

struct BrowseFlow: View {
    @Environment(\.router) var router

    private enum Path: Hashable {
        case couponDetail(Coupon)
    }

    var body: some View {
        ProductsFlow(services: .init(cartService: cartService, couponService: couponService, productService: productService),
                     couponTapHandler: { coupon in
            router.append(Path.couponDetail(coupon))
        })
        .navigationDestination(for: Path.self) {
            switch $0 {
            case .couponDetail(let coupon):
                CouponFlow(couponService: couponService, initialCoupon: coupon)
                    .routerSegment()
            }
        }
    }
}

#Preview {
    BrowseFlow()
}
