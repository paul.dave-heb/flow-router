//
//  FlowRoutersApp.swift
//  FlowRouters
//
//  Created by Paul,Dave on 11/1/23.
//

import SwiftUI

@main
struct FlowRoutersApp: App {
    var body: some Scene {
        WindowGroup {
            RouterNavigationStack {
                BrowseFlow()
            }
        }
    }
}
