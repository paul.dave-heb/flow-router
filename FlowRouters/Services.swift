//
//  ProductService.swift
//  FlowViews
//
//  Created by Paul,Dave on 10/28/23.
//

import Combine
import Foundation

struct Product: Identifiable, Hashable {
    var id: Int
}

struct FAQ: Identifiable, Hashable {
    var id: Int
}

struct Coupon: Identifiable, Hashable {
    var id: Int
}

class ProductService {
    func productListPublisher() -> some Publisher<[Product], Never> {
        let products = (0 ..< 10).map { Product(id: $0) }
        return Just(products)
    }

    func productPublisher(for id: Int) -> some Publisher<Product, Never> {
        Just(Product(id: id))
    }
}

class CouponService {
    func couponPublisher(for id: Int) -> some Publisher<Coupon, Never> {
        Just(Coupon(id: id))
    }
}

class CartService {
    func adjustQuantity(productId: Int, quantity: Int) async throws {}
}

class FAQService {
    func faqsPublisher() -> some Publisher<[FAQ], Never> {
        Just([FAQ(id: 0)])
    }

    func faqPublisher(id: Int) -> some Publisher<FAQ, Never> {
        Just(FAQ(id: 0))
    }
}
