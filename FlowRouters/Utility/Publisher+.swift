//
//  Publisher+.swift
//  FlowViews
//
//  Created by Paul,Dave on 10/28/23.
//

import Combine

extension Publisher {
    var optional: some Publisher<Output?, Failure> {
        map { $0 as Output? }
    }
}
