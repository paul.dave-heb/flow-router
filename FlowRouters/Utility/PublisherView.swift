//
//  PublisherView.swift
//  FlowViews
//
//  Created by Paul,Dave on 10/28/23.
//

import Combine
import SwiftUI

public struct PublisherView<P: Publisher, V: View>: View where P.Failure == Never {
    let publisher: Publishers.Share<Publishers.ReceiveOn<P, DispatchQueue>>
    let content: (P.Output) -> V

    @State var value: P.Output

    public init(publisher: P, initialValue: P.Output, @ViewBuilder content: @escaping (P.Output) -> V) {
        // We `share()` the publisher so that when the `body` re-renders the `View`
        // it will not cause a re-subscription, but subscribe back to the original stream.
        self.publisher = publisher.receive(on: DispatchQueue.main).share()
        self.content = content
        _value = State<P.Output>(initialValue: initialValue)
    }

    public var body: some View {
        content(value).onReceive(publisher, perform: { value = $0 })
    }
}

extension PublisherView where P.Output: ExpressibleByNilLiteral {
    public init(publisher: P, @ViewBuilder content: @escaping (P.Output) -> V) {
        self.init(publisher: publisher, initialValue: nil, content: content)
    }
}
