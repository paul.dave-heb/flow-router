//
//  RouterSegmentModifier.swift
//  FlowRouters
//
//  Created by Paul,Dave on 11/3/23.
//

import SwiftUI

public extension View {
    /// Defines a new router segment at the input view.
    /// Routers obtained through the environment within this view will not be able to drive navigation outside of the view.
    func routerSegment() -> some View {
        modifier(RouterSegmentViewModifier())
    }
}

private struct RouterSegmentViewModifier: ViewModifier {
    /// The "parent" router, handed down from the environment
    @Environment(\._router) var envRouter

    /// The modified router that is scoped to this segment
    @State var storedRouter: Router? // We capture this in @State in order to "freeze" the navigation path at the time this view appears.

    func body(content: Content) -> some View {
        content
            .environment(\._router, storedRouter ?? RouterKey.defaultValue) // Pass the modified router down to children
            .onAppear {
                if storedRouter == nil {
                    storedRouter = envRouter.child()
                }
            }
    }
}
