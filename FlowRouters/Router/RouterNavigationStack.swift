//
//  RouterNavigationStack.swift
//  FlowRouters
//
//  Created by Paul,Dave on 11/3/23.
//

import SwiftUI

/// A NavigationStack that binds its path to a Router and injects it into the environment.
struct RouterNavigationStack<Content: View>: View {
    @State var path = NavigationPath()

    let content: () -> Content

    init(content: @escaping () -> Content) {
        self.content = content
    }

    var body: some View {
        NavigationStack(path: $path) {
            content()
        }.environment(\._router, Router(navPath: $path))
    }
}
