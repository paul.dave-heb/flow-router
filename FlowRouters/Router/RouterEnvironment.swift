//
//  RouterEnvironment.swift
//  FlowRouters
//
//  Created by Paul,Dave on 11/3/23.
//

import SwiftUI

internal struct RouterKey: EnvironmentKey {
    static var defaultValue: Router {
        Router(navPath: .init(get: { NavigationPath() }, set: { _ in }))
    }
}

extension EnvironmentValues {
    /// A router that can be used to drive navigation path changes to a parent NavigationStack.
    public var router: Router {
        get { self[RouterKey.self] }
    }

    internal var _router: Router {
        get { self[RouterKey.self] }
        set { self[RouterKey.self] = newValue }
    }
}

