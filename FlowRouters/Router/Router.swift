//
//  Router.swift
//  FlowRouters
//
//  Created by Paul,Dave on 11/1/23.
//

import SwiftUI

public struct Router {

    // Private! We don't expose the full access of the navPath
    @Binding private var navPath: NavigationPath

    private let segmentRootIndex: Int

    /// The number of navigation path items in the current segment.
    public var segmentCount: Int {
        navPath.count - segmentRootIndex + 1
    }

    /// Creates a Router, bound to the given NavigationPath.
    /// Prefer using RouterNavigationStack over initializing a Router directly.
    public init(navPath: Binding<NavigationPath>) {
        _navPath = navPath
        segmentRootIndex = navPath.wrappedValue.count
    }

    /// Whether or not the current segment is the root of the navigation path.
    public var isRootSegment: Bool {
        segmentRootIndex == 0
    }

    public func append<V: Hashable>(_ value: V) {
        navPath.append(value)
    }

    public func removeLast(_ count: Int) {
        navPath.removeLast(min(segmentCount, count))
    }

    internal func child() -> Router {
        Router(navPath: $navPath)
    }

    var description: String {
        "Router - index:\(segmentRootIndex), segmentCount:\(navPath.count)"
    }
}

// MARK: - Convenience methods
public extension Router {
    /// Pops to the root of the current segment.
    func popToRoot() {
        removeLast(segmentCount - 1)
    }

    /// Pops to the navigation path item underneath the current segment.
    func dismissAll() {
        removeLast(segmentCount)
    }
}
